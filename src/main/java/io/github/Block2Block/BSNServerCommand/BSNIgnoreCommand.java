package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNIgnoreCommand extends Command {

    public BSNIgnoreCommand()
    {
        super( "ignore", "",new String[0]);
    }

    public void execute(CommandSender sender, String[] args) {
        if (args.length > 0) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            String a = p.getUniqueId().toString();
            ProxiedPlayer b = Main.i.getProxy().getPlayer(args[0]);
            if (b == null) {
                Main.cb(p, "Ignore", "Could not find player " + args[0] + "&7.");
                return;
            }
            String c = b.getUniqueId().toString();
            if (Main.i.mysql.isIgnoredBy(a, c)) {
                Main.i.mysql.removeIgnored(a,c);
                Main.cb(p,"Ignore","You are no longer ignoring &a" + b.getName() + "&7.");
                return;
            }
            if (b.hasPermission("rank.staff")) {
                Main.cb(p,"Ignore","You are not allowed to ignore staff members.");
                return;
            }
            Main.i.mysql.addIgnored(a, c);
            Main.cb(p,"Ignore","You are now ignoring &a" + b.getName() + "&7.");
        }
    }
}
