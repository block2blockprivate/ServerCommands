package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class MapHandeling {
    private ProxiedPlayer playername; 
	private String lastActivity;

    public void Message(ProxiedPlayer player, String lastActivity) {
        this.playername = player;
        this.lastActivity = lastActivity;
    }

    public ProxiedPlayer getPlayer() {
        return this.playername;
    }

    public String getActivity() {
        return this.lastActivity;
    }
}
