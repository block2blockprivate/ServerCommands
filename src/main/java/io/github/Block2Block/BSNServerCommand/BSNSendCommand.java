package io.github.Block2Block.BSNServerCommand;

import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;

public class BSNSendCommand extends Command implements TabExecutor{


    public BSNSendCommand(){
        super("send","bungeecord.command.send",new String[0]);
    }
        @Override
        public void execute(CommandSender sender, String[] args)
        {
            if ( args.length != 2 )
            {
                sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "Not enough arguments!").color(ChatColor.RED).create());
                return;
            }
            ServerInfo target = ProxyServer.getInstance().getServerInfo( args[1] );
            if ( target == null )
            {
                sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "That server does not exist!").color(ChatColor.RED).create());
                return;
            }

            if ( args[0].equalsIgnoreCase( "all" ) )
            {
                for ( ProxiedPlayer p : ProxyServer.getInstance().getPlayers() )
                {
                    summon( p, target, sender );
                }
            } else if ( args[0].equalsIgnoreCase( "current" ) )
            {
                if ( !( sender instanceof ProxiedPlayer ) )
                {
                    sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "Only players can use current!").color(ChatColor.RED).create());
                    return;
                }
                ProxiedPlayer player = (ProxiedPlayer) sender;
                for ( ProxiedPlayer p : player.getServer().getInfo().getPlayers() )
                {
                    summon( p, target, sender );
                }
            } else
            {
                // If we use a server name, send the entire server. This takes priority over players.
                ServerInfo serverTarget = ProxyServer.getInstance().getServerInfo( args[0] );
                if ( serverTarget != null )
                {
                    for ( ProxiedPlayer p : serverTarget.getPlayers() )
                    {
                        summon( p, target, sender );
                    }
                } else
                {
                    ProxiedPlayer player = ProxyServer.getInstance().getPlayer( args[0] );
                    if ( player == null )
                    {
                        sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "That player is not online!").color(ChatColor.RED).create());
                        return;
                    }
                    summon( player, target, sender );
                }
            }
            sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GRAY + "Successfull summoned player(s)!").color(ChatColor.GRAY).create());
        }

        private void summon(ProxiedPlayer player, ServerInfo target, CommandSender sender)
        {
            if ( player.getServer() != null && !player.getServer().getInfo().equals( target ) )
            {
                player.connect( target );
                player.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GRAY + "You are being sent from " + ChatColor.GREEN + player.getServer().getInfo().getName() + ChatColor.GRAY + " to " + ChatColor.GREEN + target.getName() + ChatColor.GRAY + " by " + ChatColor.GREEN + sender.getName() + ChatColor.GREEN + "!").color(ChatColor.GRAY).create());
            }
        }

        @Override
        public Iterable<String> onTabComplete(CommandSender sender, String[] args)
        {
            if ( args.length > 2 || args.length == 0 )
            {
                return ImmutableSet.of();
            }

            Set<String> matches = new HashSet<>();
            if ( args.length == 1 )
            {
                String search = args[0].toLowerCase();
                for ( ProxiedPlayer player : ProxyServer.getInstance().getPlayers() )
                {
                    if ( player.getName().toLowerCase().startsWith( search ) )
                    {
                        matches.add( player.getName() );
                    }
                }
                if ( "all".startsWith( search ) )
                {
                    matches.add( "all" );
                }
                if ( "current".startsWith( search ) )
                {
                    matches.add( "current" );
                }
            } else
            {
                String search = args[1].toLowerCase();
                for ( String server : ProxyServer.getInstance().getServers().keySet() )
                {
                    if ( server.toLowerCase().startsWith( search ) )
                    {
                        matches.add( server );
                    }
                }
            }
            return matches;
        }
	
}
