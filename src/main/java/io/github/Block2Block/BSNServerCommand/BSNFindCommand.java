package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNFindCommand extends Command {

    public BSNFindCommand()
    {
        super( "find", "bungeecord.command.find",new String[0]);
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if ( args.length != 1 )
        {
            sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "Not enough arguments!").color(ChatColor.RED).create());
        } else
        {
            ProxiedPlayer player = ProxyServer.getInstance().getPlayer( args[0] );
            if ( player == null || player.getServer() == null )
            {
                sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GRAY + "No results found for " + ChatColor.GREEN + args[0] + ChatColor.GRAY + ".").color(ChatColor.GRAY).create());
            } else
            {
                sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GREEN + args[0] + ChatColor.GRAY + " is online at " + ChatColor.GREEN + player.getServer().getInfo().getName() + ChatColor.GRAY + ".").color(ChatColor.GRAY).create());
            }
        }
    }
	
}
