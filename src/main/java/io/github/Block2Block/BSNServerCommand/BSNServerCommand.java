package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNServerCommand extends Command {

    public BSNServerCommand(){
        super("server","",new String[0]);
    }
    public void execute(CommandSender sender, String[] args){
    	if(sender instanceof ProxiedPlayer){
			ProxiedPlayer player = (ProxiedPlayer) sender;
			if (args.length == 1) {
				if(!player.getServer().getInfo().getName().equalsIgnoreCase(args[0])){
					ServerInfo target = ProxyServer.getInstance().getServerInfo(args[0]);
					if (target == null) {
						player.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "That server does not exist!").color(ChatColor.RED).create());
					}else {
						if(!target.canAccess(player)){
                            player.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GRAY + "You are not allowed to join that server!").color(ChatColor.GRAY).create());
                            return;
						}
						player.connect(target);
						player.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GRAY + "You are being sent from " + ChatColor.GREEN + player.getServer().getInfo().getName() + ChatColor.GRAY + " to " + ChatColor.GREEN + ProxyServer.getInstance().getServerInfo(args[0]).getName() + ChatColor.GRAY + "!").color(ChatColor.GRAY).create());
					}
				}else{
					player.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "You are already connected to that server!").color(ChatColor.RED).create());
				}
			}else {
				player.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.GRAY + "You are currently connected to " + ChatColor.GREEN + player.getServer().getInfo().getName() + ChatColor.GRAY + ".").color(ChatColor.GRAY).create());
			}
    	}else{
    		sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Server Management>> " + ChatColor.RED + "This command can only be run by a player!").color(ChatColor.RED).create());
    }
    }
	
}
