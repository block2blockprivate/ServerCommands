package io.github.Block2Block.BSNServerCommand;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Plugin;

import java.sql.SQLException;


public class Main extends Plugin {

    public static Main i;

    public MySQLManager mysql;


    public void onEnable() {
        i = this;
        getProxy().getPluginManager().registerCommand(this, new BSNServerCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNSendCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNFindCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNAlertCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNMsgCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNReplyCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNSocialSpyCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNIgnoreCommand());
        getProxy().getPluginManager().registerCommand(this, new BSNHubCommand());

        mysql = new MySQLManager();
        try {
            mysql.setup();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }

    public static String c(@Nullable String prefix, @NotNull String message) {
        return ChatColor.translateAlternateColorCodes('&', ((prefix == null) ? "&7" : "&2" + prefix + ">> &7") + message);
    }

    public static void cb(ProxiedPlayer player, String prefix, String message) {
        player.sendMessage(new ComponentBuilder(c(prefix, message)).color(ChatColor.GRAY).create());
    }
}
