package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.config.ServerInfo;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNHubCommand extends Command {

    public BSNHubCommand(){
        super("hub","",new String[]{"lobby", "home"});

    }
    public void execute(CommandSender sender, String[] args){
        if (sender instanceof ProxiedPlayer) {
            ServerInfo target = ProxyServer.getInstance().getServerInfo("Hub");
            if (!((ProxiedPlayer) sender).getServer().getInfo().getName().equalsIgnoreCase("hub")) {
                ((ProxiedPlayer) sender).connect(target);
                Main.cb((ProxiedPlayer) sender, "Server Management","You are being sent from &a" + ((ProxiedPlayer) sender).getServer().getInfo().getName() + " &7to &aHub&7.");
            } else {
                Main.cb((ProxiedPlayer) sender, "Server Management","You are already connected to this server!");
            }
        } else {
            Main.cb((ProxiedPlayer) sender,"Hub","You can only execute this command from in-game.");
        }
    }

}
