package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNReplyCommand extends Command {
	
	private static ProxyServer ps = ProxyServer.getInstance();
	
	public BSNReplyCommand()
    {
        super( "reply", "",new String[]{"r"});
    }

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length == 0) {
            sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "Incorrect syntax.").color(ChatColor.GRAY).create());
		} else {
        	if (BSNMsgCommand.getMap().containsKey(ps.getPlayer(sender.getName()))) {
    	        ProxiedPlayer player = ProxyServer.getInstance().getPlayer(BSNMsgCommand.getLast(ps.getPlayer(sender.getName())));	      
    	        if ( player == null || player.getServer() == null ){
    	            sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "That player is not online.").color(ChatColor.GRAY).create());
    	        } else {
    	        	if (player != sender) {
    		        	String string = "";
    		        	for (int i = 0; i<args.length; i++) {
    		        		string += args[i]+" ";
    		        	}
    		        	string = string.trim();
    		        	
    		        	sender.sendMessage(new ComponentBuilder(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "(" + ChatColor.RESET + "" + ChatColor.AQUA + "" + ChatColor.BOLD + sender.getName() + ChatColor.RESET + " " + ChatColor.GRAY + "" + ChatColor.BOLD + "->" + ChatColor.RESET + " " + ChatColor.AQUA + "" + ChatColor.BOLD + player.getName() + ChatColor.RESET + "" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + ")" + ChatColor.RESET + " " + ChatColor.GRAY + string).color(ChatColor.GRAY).create());
    		        	player.sendMessage(new ComponentBuilder(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "(" + ChatColor.RESET + "" + ChatColor.AQUA + "" + ChatColor.BOLD + sender.getName() + ChatColor.RESET + " " + ChatColor.GRAY + "" + ChatColor.BOLD + "->" + ChatColor.RESET + " " + ChatColor.AQUA + "" + ChatColor.BOLD + player.getName() + ChatColor.RESET + "" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + ")" + ChatColor.RESET + " " + ChatColor.GRAY + string).color(ChatColor.GRAY).create());
    		        	BSNMsgCommand.getMap().remove(ps.getPlayer(sender.getName()));
    		        	BSNMsgCommand.getMap().put(ps.getPlayer(sender.getName()), player.getName());
    		        	BSNMsgCommand.getMap().remove(player);
    		        	BSNMsgCommand.getMap().put(player, sender.getName());
    		        	for(ProxiedPlayer p: BSNSocialSpyCommand.list) {
    		        		try {
								p.sendMessage(new ComponentBuilder(Main.c("SocialSpy", sender.getName() + " -> " + player.getName() + ": &a" + string)).color(ChatColor.GRAY).create());
							} catch (Throwable e) {
							}
						}
    	        	} else {
    	        		sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "You can't message yourself!").color(ChatColor.GRAY).create());
    	        	}
    	       }
	        } else {
	        	sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "You haven't messaged anyone recently.").color(ChatColor.GRAY).create());
	        }
		}
		
	}
	
}
