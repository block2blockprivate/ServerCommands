package io.github.Block2Block.BSNServerCommand;

import io.github.Block2Block.BSNServerCommand.MySQL.MySQL;
import net.md_5.bungee.api.chat.ComponentBuilder;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class MySQLManager {

    public static MySQLManager i;

    private static MySQL db;

    public MySQLManager() {
        i = this;
    }

    public void setup() throws SQLException, ClassNotFoundException {
        db = new MySQL("172.106.202.99", "3306","SimplyBrandon1_Ignore", "SimplyBrandon1", "Password123123");
        db.openConnection();
    }

    public boolean addIgnored(String uuid, String uuid2) {
        try {
            Statement statement = db.getConnection().createStatement();
            statement.executeUpdate("CREATE TABLE IF NOT EXISTS `" + uuid + "` (`uuid` varchar(36))");
            if (isIgnoredBy(uuid, uuid2)) {
                statement.close();
                return false;
            }
            statement.executeUpdate("INSERT INTO `" + uuid + "` (`uuid`) VALUES ('" + uuid2 + "')");
            statement.close();
            return true;
        } catch (SQLException t) {
            t.printStackTrace();
            return true;
        }
    }

    public boolean removeIgnored(String uuid, String uuid2) {
        try {
            Statement statement = db.getConnection().createStatement();
            if (!isIgnoredBy(uuid, uuid2)) {
                statement.close();
                return false;
            }
            statement.executeUpdate("DELETE FROM `" + uuid + "` WHERE uuid='" + uuid2 + "'");
            statement.close();
            return true;
        } catch (SQLException t) {
            t.printStackTrace();
            return true;
        }
    }

    public static boolean isIgnoredBy(String uuid, String uuid2) {
        try {
            Statement statement = db.getConnection().createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM `" + uuid + "`");
            List<String> uuids = new ArrayList<>();
            int i = 1;
            while (set.next()) {
                uuids.add(set.getString(i));
            }
            if (uuids.contains(uuid2)) {
                statement.close();
                return true;
            }
            return false;
        } catch (SQLException t) {
            t.printStackTrace();
            return false;
        }
    }

    public List<String> getIgnored(String uuid) {
        List<String> list = new ArrayList<>();
        try {
            Statement statement = db.getConnection().createStatement();
            ResultSet set = statement.executeQuery("SELECT * FROM `" + uuid + "`");
            int i = 1;
            while (set.next()) {
                list.add(set.getString(i));
            }
            return list;
        } catch (SQLException t) {
            t.printStackTrace();
            return list;
        }
    }
}
