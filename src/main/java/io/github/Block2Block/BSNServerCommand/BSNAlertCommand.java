package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNAlertCommand extends Command {

	public BSNAlertCommand(){
        super("announce","bungeecord.command.alert",new String[0]);
    }

	@Override
	public void execute(CommandSender sender, String[] args) {
		// TODO Auto-generated method stub
		Title title = ProxyServer.getInstance().createTitle();
		title.fadeIn(10);
        title.stay(100);
        title.fadeOut(10);
        TextComponent text = new TextComponent(ChatColor.DARK_GREEN + "" + ChatColor.BOLD + "ANNOUNCEMENT");
        title.title(text);
        String t = String.join(" ",args);
        TextComponent text2 = new TextComponent(t);
        title.subTitle(text2);
        if(args.length < 1){
        	sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Announcement>> " + ChatColor.RED + "Invalid Arguments!").color(ChatColor.RED).create());
        } else {
            for(ProxiedPlayer x : ProxyServer.getInstance().getPlayers()) {
            	title.send(x);
            	x.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Announcement>> " + ChatColor.GRAY + t).color(ChatColor.GRAY).create());
            }
        }
        title.reset();
	}

}
