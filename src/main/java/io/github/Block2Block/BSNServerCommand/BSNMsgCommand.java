package io.github.Block2Block.BSNServerCommand;

import java.util.HashMap;
import java.util.Map;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class BSNMsgCommand extends Command {
	
	private static Map<ProxiedPlayer, String> lastSent = new HashMap<>();
	private static ProxyServer ps = ProxyServer.getInstance();
	
	public BSNMsgCommand(){
        super("msg","",new String[]{"message", "w", "whisper", "tell", "m", "t"});
        
    }

	@Override
	public void execute(CommandSender sender, String[] args) {
		if (args.length <= 1) {
            sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "Incorrect syntax.").color(ChatColor.GRAY).create());
		} else {
	        ProxiedPlayer player = ProxyServer.getInstance().getPlayer( args[0] );	      
	        if ( player == null || player.getServer() == null ){
	            sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "That player is not online.").color(ChatColor.GRAY).create());
	        } else {
	        	if (player != sender) {
	        		/*
		        	List<String> list = new ArrayList<String>(Arrays.asList(args));
		        	list.remove(player.getName());
		        	args = list.toArray(new String[0]);
		     
		        	*/
	        		
		        	String string = "";
		        	for (int i = 1; i<args.length; i++) {
		        		string += args[i]+" ";
		        	}
		        	string = string.trim();
		        	ProxiedPlayer p2 = (ProxiedPlayer) sender;

		        	if (MySQLManager.isIgnoredBy(player.getUniqueId().toString(),p2.getUniqueId().toString())) {
		        		((ProxiedPlayer) sender).sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "You cannot message people that are ignoring you.").color(ChatColor.GRAY).create());
		        		return;
					}
		        	
		        	sender.sendMessage(new ComponentBuilder(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "(" + ChatColor.RESET + "" + ChatColor.AQUA + "" + ChatColor.BOLD + sender.getName() + ChatColor.RESET + " " + ChatColor.GRAY + "" + ChatColor.BOLD + "->" + ChatColor.RESET + " " + ChatColor.AQUA + "" + ChatColor.BOLD + player.getName() + ChatColor.RESET + "" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + ")" + ChatColor.RESET + " " + ChatColor.GRAY + string).color(ChatColor.GRAY).create());
		        	player.sendMessage(new ComponentBuilder(ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "(" + ChatColor.RESET + "" + ChatColor.AQUA + "" + ChatColor.BOLD + sender.getName() + ChatColor.RESET + " " + ChatColor.GRAY + "" + ChatColor.BOLD + "->" + ChatColor.RESET + " " + ChatColor.AQUA + "" + ChatColor.BOLD + player.getName() + ChatColor.RESET + "" + ChatColor.DARK_AQUA + "" + ChatColor.BOLD + ")" + ChatColor.RESET + " " + ChatColor.GRAY + string).color(ChatColor.GRAY).create());
		        	lastSent.remove(ps.getPlayer(sender.getName()));
		        	lastSent.put(ps.getPlayer(sender.getName()), player.getName());
		        	lastSent.remove(player);
		        	lastSent.put(player, sender.getName());
					for(ProxiedPlayer p: BSNSocialSpyCommand.list) {
						try {
							p.sendMessage(new ComponentBuilder(Main.c("SocialSpy", sender.getName() + " -> " + player.getName() + ": &a" + string)).color(ChatColor.GRAY).create());
						} catch (Throwable e) {
						}
					}
	        	} else {
	        		sender.sendMessage(new ComponentBuilder(ChatColor.DARK_GREEN + "Msg>> " + ChatColor.GRAY + "You can't message yourself!").color(ChatColor.GRAY).create());
	        	}
	        }
		}
	}
	
	public static String getLast(ProxiedPlayer p) {
		return lastSent.get(p);
	}
	
	public static Map<ProxiedPlayer, String> getMap() {
		return lastSent;
	}

}
