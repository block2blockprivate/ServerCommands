package io.github.Block2Block.BSNServerCommand;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.List;

public class BSNSocialSpyCommand extends Command {

    public BSNSocialSpyCommand()
    {
        super( "socialspy", "bungeecord.command.send",new String[]{"ss"});
    }
    public static List<ProxiedPlayer> list = new ArrayList<>();

    public void execute(CommandSender sender, String[] args) {
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer p = (ProxiedPlayer) sender;
            if (list.contains(p)) {
                list.remove(p);
                p.sendMessage(new ComponentBuilder(Main.c("SocialSpy","You have &cdisabled &7Command Spy.")).color(ChatColor.GRAY).create());
            } else {
                list.add(p);
                p.sendMessage(new ComponentBuilder(Main.c("SocialSpy","You have &aenabled &7Command Spy.")).color(ChatColor.GRAY).create());
            }
        } else {
            sender.sendMessage(new ComponentBuilder(Main.c("SocialSpy","This command can only be executed in-game.")).color(ChatColor.GRAY).create());
        }
    }
}
